# 忧慧购

## 介绍

- 忧慧购优惠购，忧对优慧对惠，无需忧愁绝对优惠，绝对实惠放心购。
- 以极简-极速-便捷为主，打造简单而不是高端的购物商场，致力于微信小程序开发的Lite商场

## 系统框架&功能列表
![](忧慧购.png)

## 简要详说功能

### 购物流程

1. 商城购物分为⽅式：加⼊购物车

    - 加⼊购物车：添加多个商品信息（购物车id，商品id，商品数量、添加时间、下单时间、支付时间）到购物车⾥，统⼀下单处理；存储到stroge中,并清除购物车中选中的商品，选中的收货地址等，⽣成订单时通过stroge读取商品信息在订单完成后，清除stroge信息

    - 购物车到确认订单时，将购物车选中的商品通过stroge将购物车id存储到stroge，----》确认订单页⾯通过stroge读取商品信息展⽰
    
2. 订单提交开启分支付了和没支付状态，开启定时器并和JSON数据相关联，到期支付失败，期限内支付成功转至待发货转态

3. 有一次性催发货功能，确认收货功能，评价功能

3. 在开发过程中所有数据都以JSON信息为准，避免网络问题引起重复bug。

### 地址管理流程

分为两种情况：

1. 购物时没有收货地址提示添加
    - 点击需要邮寄的地址右边的切换按钮。
    - 界面跳至地址页面
    - 然后点击底部收货地址添加选项。
    - 弹出弹窗然后填写相关信息电话姓名地址，全部为必填项。
    - 最后点击添加按钮保存，保存填写好的购物地址。
    - 点击左上角返回按钮回到下单页

2. 手动添加收货地址
    - 登录账号后，点击我的页-地址管理选项。
    - 然后点击底部收货地址添加选项。
    - 弹出弹窗然后填写相关信息电话姓名地址，全部为必填项。
    - 最后点击添加按钮保存，保存填写好的购物地址。
    - 然后在商品付款页面，可以点击需要邮寄的地址右边的切换按钮。
    - 底部弹出地址选项，点击需要收货的地址即可切换地址


## 功能截图

### address地址管理

<img width="200" src="./pic/address/pic%20(1).jpg" />
<img width="200" src="./pic/address/pic%20(2).jpg" />
<img width="200" src="./pic/address/pic%20(3).jpg" />

### car&pay 购物车 下单支付

<img width="200" src="./pic//carPay/pic%20(1).jpg" />
<img width="200" src="./pic//carPay/pic%20(2).jpg" />
<img width="200" src="./pic//carPay/pic%20(3).jpg" />
<img width="200" src="./pic//carPay/pic%20(4).jpg" />

### category分类页

<img width="200" src="./pic/category/pic%20(1).jpg" />
<img width="200" src="./pic/category/pic%20(2).jpg" />

### detail物品详情

<img width="200" src="./pic/detail/pic%20(1).jpg" />
<img width="200" src="./pic/detail/pic%20(2).jpg" />

### home首页

<img width="200" src="./pic/home/pic%20(1).jpg" />
<img width="200" src="./pic/home/pic%20(2).jpg" />

### my我的页

<img width="200" src="./pic/my/pic%20(1).jpg" />
<img width="200" src="./pic/my/pic%20(2).jpg" />
<img width="200" src="./pic/my/pic%20(3).jpg" />

### order订单页

<img width="200" src="./pic/order/pic%20(1).jpg" />
<img width="200" src="./pic/order/pic%20(2).jpg" />
<img width="200" src="./pic/order/pic%20(3).jpg" />

### result搜索结果&物品列表

<img width="200" src="./pic/result/pic%20(1).jpg" />
<img width="200" src="./pic/result/pic%20(2).jpg" />
<img width="200" src="./pic/result/pic%20(3).jpg" />

### search搜索页

<img width="200" src="./pic/search/pic%20(1).jpg" />
<img width="200" src="./pic/search/pic%20(2).jpg" />
<img width="200" src="./pic/search/pic%20(3).jpg" />
