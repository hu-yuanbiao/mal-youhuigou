module.exports = {
    imgs: [
        {
            "w": 720,
            "h": 360,
            "material_id": 34695,
            "ad_position_id": 3806,
            "img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/4491003159d13cff750f3d89d1ac032c.jpg?f=webp&w=1080&h=540&bg=FFFFFF",
            "img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/4491003159d13cff750f3d89d1ac032c.jpg?f=webp&w=1080&h=540&bg=FFFFFF",
            "img_url_color": "#FFFFFF",
            "action": {
                "type": "product",
                "path": "16646",
                "log_code": "31waphomegallery001003#t=ad&act=product&page=home&pid=16646&page_id=26&bid=3635492.1&adp=3806&adm=34695",
                "productId": "16646",
                "spm_stat": {
                    "spm_code": "cms_26.3635492.1",
                    "spm_params": "{\"component\":\"gallery\",\"component_name\":\"\\u8f6e\\u64ad\\u56fe 1080*540\",\"default_goods\":\"16646\",\"img\":765686}",
                    "scm": "cms.0.0.0.product.16646.0.0"
                }
            },
            "margin_left": 0,
            "margin_right": 0,
            "type": "image",
            "ad_level_id": 0
        },
        {
            "w": 720,
            "h": 360,
            "material_id": 34680,
            "ad_position_id": 2458,
            "img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1b5ff94bc655fce5506a39d2db5d079b.jpg?f=webp&w=1080&h=540&bg=FFFFFF",
            "img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1b5ff94bc655fce5506a39d2db5d079b.jpg?f=webp&w=1080&h=540&bg=FFFFFF",
            "img_url_color": "#FFFFFF",
            "action": {
                "type": "product",
                "path": "10050020",
                "log_code": "31waphomegallery002003#t=ad&act=product&page=home&pid=10050020&page_id=26&bid=3635492.2&adp=2458&adm=34680",
                "productId": "10050020",
                "spm_stat": {
                    "spm_code": "cms_26.3635492.2",
                    "spm_params": "{\"component\":\"gallery\",\"component_name\":\"\\u8f6e\\u64ad\\u56fe 1080*540\",\"default_goods\":\"10050020\",\"img\":765702}",
                    "scm": "cms.0.0.0.product.10050020.0.0"
                }
            },
            "margin_left": 0,
            "margin_right": 0,
            "type": "image",
            "ad_level_id": 0
        },
        {
            "w": 720,
            "h": 360,
            "material_id": 34669,
            "ad_position_id": 3807,
            "img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cb47b67caea653d0b49fa1df387a8fee.jpg?f=webp&w=1080&h=540&bg=FBE2C4",
            "img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cb47b67caea653d0b49fa1df387a8fee.jpg?f=webp&w=1080&h=540&bg=FBE2C4",
            "img_url_color": "#FBE2C4",
            "action": {
                "type": "crowd_home",
                "path": "https://m.mi.com/crowdfunding/home",
                "log_code": "31waphomegallery003003#t=ad&act=other&page=home&page_id=26&bid=3635492.3&adp=3807&adm=34669",
                "spm_stat": {
                    "spm_code": "cms_26.3635492.3",
                    "spm_params": "{\"component\":\"gallery\",\"component_name\":\"\\u8f6e\\u64ad\\u56fe 1080*540\",\"img\":763835}",
                    "scm": "cms.0.0.0.page.crowdfunding.0.0"
                }
            },
            "margin_left": 0,
            "margin_right": 0,
            "type": "image",
            "ad_level_id": 0
        }
    ],
    category: [
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/b1749080cf5bbc4dfebff83013bbebaf.png?f=webp&w=216&h=228&bg=FFFFFF"
        },
        {
            "img": "//i8.mifile.cn/v1/a1/eb5024fe-dfe3-6e53-3e18-675bef5fa06e.webp?w=216&h=228&bg=EAF6FD"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/69c250436545049ccab81c3e32033cf2.png?f=webp&w=216&h=228&bg=FFFFFF"
        },
        {
            "img": "//i8.mifile.cn/v1/a1/e8bc849a-0a3b-21a0-6810-7da3a3903dee.webp?w=216&h=228&bg=FDEFDE"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0434594382110f3bd15c90f040d5d542.jpg?f=webp&w=216&h=228&bg=FFFFFF"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/64f3988b6216e4c1ab62a7f50df3e816.png?f=webp&w=216&h=228&bg=FFFFFF"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/9ea68dee2bfa0e55a82236b0d968e975.png?w=216&h=228&bg=FCEAEA"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/96c780016ea196743905dc93f9249c39.png?w=216&h=228&bg=FDF5E5"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/9425031cdd7af22d9a23a5ae16d1f57c.jpg?f=webp&w=216&h=228&bg=FFFFFF"
        },
        {
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/f11f9df6b0b0b428f8c8fc3267131830.png?w=216&h=228&bg=FDEDE8"
        }
    ],
    goodsLists: [
        {
            "name": "精选",
            "title": "商品推荐",
            "lists": [
                {
                    "id": "9836",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a6a8abf50e2134139e24c30c9293871c.jpg",
                    "name": "小米巨能写中性笔10支装",
                    "price": "9.99",
                    "priceOld": "9.99"
                },
                {
                    "id": "15962",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208112039_286f538db6a6888971cf9a9abea357f1.png",
                    "name": "Redmi Note 11T Pro",
                    "price": "1569",
                    "priceOld": "1799"
                },
                {
                    "id": "11174",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3eead59acc600bf06eb38899a08bfa09.jpg",
                    "name": "小米移动电源3 20000mAh USB-C双向快充版",
                    "price": "129",
                    "priceOld": "129"
                },
                {
                    "id": "13211",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eeb0b75372a5bdb433933c624b95a182.jpg",
                    "name": "小米巨能写多彩中性笔",
                    "price": "9.99",
                    "priceOld": "9.99"
                },
                {
                    "id": "12256",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a6ed533c0a74e3d2889804c1290df557.jpg",
                    "name": "小米移动电源3 30000mAh 快充版",
                    "price": "199",
                    "priceOld": "199"
                },
                {
                    "id": "9848",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c70d927ca6b19473bf0b5b74d68da7e2.jpg",
                    "name": "小米中性笔",
                    "price": "24.9",
                    "priceOld": "24.9"
                },
                {
                    "id": "10295",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3f52a1284a94b56b6bc98f8da379221e.jpg",
                    "name": "小米USB-C数据线 100cm",
                    "price": "9.9",
                    "priceOld": "9.9"
                },
                {
                    "id": "12064",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/447aa7e179e7b6a57e83539ec3d101ba.jpg",
                    "name": "米家超级电池4粒装",
                    "price": "19.9",
                    "priceOld": "19.9"
                },
                {
                    "id": "10721",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/44a4ddd6d7abe2f3bb34a19ee5e40724.jpg",
                    "name": "小米无线充电宝 10000mAh 10W",
                    "price": "139",
                    "priceOld": "149"
                },
                {
                    "id": "15965",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205240036_9a8fbf274c0d419c5a0a49ef67cdd7c8.png",
                    "name": "小米手环7",
                    "price": "229",
                    "priceOld": "249"
                },
                {
                    "id": "15217",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/dbe2e952e307cb3245994f247aa33ea6.jpg",
                    "name": "Redmi Buds 3 青春版",
                    "price": "99",
                    "priceOld": "99"
                },
                {
                    "id": "16180",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202207011841_084ed41d67f248677914605b73faf582.png",
                    "name": "Xiaomi 12 Pro 天玑版",
                    "price": "2999",
                    "priceOld": "3999"
                },
                {
                    "id": "15964",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205211440_011c1ed4a7a41dab50eee1925c969d05.png",
                    "name": "Redmi Note 11SE",
                    "price": "1099",
                    "priceOld": "1099"
                },
                {
                    "id": "13588",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/262113025c294377934619443e510320.jpg",
                    "name": "Xiaomi 10S",
                    "price": "2399",
                    "priceOld": "2699"
                },
                {
                    "id": "15769",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/23cbaa27139108bd07a30a373a53dc6a.jpg",
                    "name": "Redmi 10A",
                    "price": "699",
                    "priceOld": "699"
                },
                {
                    "id": "14789",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e93e55dd62b3231e4db5b091b49da7f3.jpg",
                    "name": "Xiaomi MIX 4",
                    "price": "4199",
                    "priceOld": "4199"
                },
                {
                    "id": "13122",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/94701bdf46c084d26f572287a930d869.jpg",
                    "name": "Redmi Note 9 5G",
                    "price": "1199",
                    "priceOld": "1199"
                },
                {
                    "id": "7530",
                    "img": "//i8.mifile.cn/b2c-mimall-media/72f7329a96a5c89bf160fba8cd2745be.jpg",
                    "name": "知吾煮汤锅 米家定制",
                    "price": "99",
                    "priceOld": "99"
                },
                {
                    "id": "16337",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208111030_72ac87e23acbfcb42873e4633e2ec479.jpg",
                    "name": "Xiaomi MIX Fold 2",
                    "price": "8999",
                    "priceOld": "8999"
                },
                {
                    "id": "16642",
                    "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202210262033_ef39fca0e37395d07682124770fd3ad9.png",
                    "name": "Redmi Note 12 5G",
                    "price": "1199",
                    "priceOld": "1199"
                }
            ]
        },
        {
            "name": "手机",
            "title": "探索黑科技",
            "lists": [
                {
                    "id": "16326",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208101511_488638d8f8d5dbcf3b66cd82703ecfb9.png",
                    "name": "Redmi K50 至尊版",
                    "price": "2999",
                    "priceOld": "2999"
                },
                {
                    "id": "15135",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cb3bfa91499d394b23f144d36fb819d5.png",
                    "name": "Redmi Note 11 5G",
                    "price": "1099",
                    "priceOld": "1199"
                },
                {
                    "id": "15962",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208112039_cb21539a05b3926928b9c4d8295f2c57.png",
                    "name": "Redmi Note 11T Pro",
                    "price": "1569",
                    "priceOld": "1799"
                },
                {
                    "id": "15769",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/29ada61b2cc607c16e8cfe360275cff6.png",
                    "name": "Redmi 10A",
                    "price": "699",
                    "priceOld": "699"
                },
                {
                    "id": "16644",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202210262012_94dd4ca657adcebec0d11ea09dac8a03.png",
                    "name": "Redmi Note 12 Pro",
                    "price": "1699",
                    "priceOld": "1699"
                },
                {
                    "id": "16642",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202210262033_ef39fca0e37395d07682124770fd3ad9.png",
                    "name": "Redmi Note 12 5G",
                    "price": "1199",
                    "priceOld": "1199"
                },
                {
                    "id": "15491",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/eac4850d9dbc5305b1298b8c1c686c17.png",
                    "name": "Xiaomi 12",
                    "price": "3699",
                    "priceOld": "3699"
                },
                {
                    "id": "15415",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0593c1e9e06a125b9e22c1041aa0a85d.png",
                    "name": "Xiaomi 11 青春活力版",
                    "price": "1799",
                    "priceOld": "1999"
                },
                {
                    "id": "15694",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/268eb39e48af6bb86e448e143a119eea.png",
                    "name": "Redmi K50",
                    "price": "2299",
                    "priceOld": "2399"
                },
                {
                    "id": "15133",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/14031a460b7a755d5371225434a0a123.png",
                    "name": "Redmi Note 11 Pro",
                    "price": "1599",
                    "priceOld": "1799"
                },
                {
                    "id": "16178",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202207012026_c5e0f5b141c708d84a44764d066717af.png",
                    "name": "Xiaomi 12S",
                    "price": "3699",
                    "priceOld": "3999"
                },
                {
                    "id": "15472",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bec639601906ed7649970c6ab311f992.png",
                    "name": "Xiaomi 12 Pro",
                    "price": "4699",
                    "priceOld": "4699"
                },
                {
                    "id": "15959",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208112039_cb21539a05b3926928b9c4d8295f2c57.png",
                    "name": "Redmi Note 11T Pro+",
                    "price": "1899",
                    "priceOld": "2099"
                },
                {
                    "id": "14999",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2fb0f98dd723b9a3b66167aa7e86a8ea.png",
                    "name": "Xiaomi Civi",
                    "price": "2099",
                    "priceOld": "2299"
                },
                {
                    "id": "16536",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202209261921_a1b840c267bd26bcf4dc654d52f259e5.png",
                    "name": "Xiaomi Civi 2",
                    "price": "2399",
                    "priceOld": "2399"
                },
                {
                    "id": "16337",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208111030_e3554c41e0484da99b16bb9e02142e68.png",
                    "name": "Xiaomi MIX Fold 2",
                    "price": "8999",
                    "priceOld": "8999"
                },
                {
                    "id": "13873",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b0e55e428c57a718fd87d23cce939637.png",
                    "name": "Xiaomi 11 青春版",
                    "price": "1799",
                    "priceOld": "1999"
                },
                {
                    "id": "15792",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f77f2b5c0a09d53f739e1cffe3394ca3.png",
                    "name": "黑鲨5 RS",
                    "price": "2499",
                    "priceOld": "2799"
                },
                {
                    "id": "15136",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4b96f308ef704cf93068c41233925364.png",
                    "name": "Redmi Note 11 Pro+",
                    "price": "1799",
                    "priceOld": "1999"
                },
                {
                    "id": "15791",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/29d255503783b6838beee713dd5630ec.png",
                    "name": "黑鲨5 Pro",
                    "price": "3499",
                    "priceOld": "4199"
                }
            ]
        },
        {
            "name": "电视",
            "title": "数码电器",
            "lists": [
                {
                    "id": "13936",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f8c4e3754081c513a791b17c64070e42.png",
                    "name": "小米电视EA55 2022款55英寸",
                    "price": "1399",
                    "priceOld": "2099"
                },
                {
                    "id": "13933",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3a8e22d6a78a20c042834456237d85cf.png",
                    "name": "小米电视EA43 2022款43英寸",
                    "price": "799",
                    "priceOld": "1399"
                },
                {
                    "id": "14023",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202211112300_8a6e93d63ee1d680517ea3728da56503.jpg",
                    "name": "小米电视EA75 2022款75英寸",
                    "price": "2999",
                    "priceOld": "4199"
                },
                {
                    "id": "13920",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6e27a6ab4209b9731bcdd88f42331121.png",
                    "name": "小米电视EA32 2022款32英寸",
                    "price": "599",
                    "priceOld": "899"
                },
                {
                    "id": "13937",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202111112019_363bf63e1ca2c6b21a0e5e21cd284368.png",
                    "name": "小米电视EA65 2022款65英寸",
                    "price": "1999",
                    "priceOld": "2899"
                },
                {
                    "id": "15734",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a41d2c0c8e7c5248e7ee65f1eadf28a6.png",
                    "name": "Redmi MAX 100\" 巨屏电视",
                    "price": "19999",
                    "priceOld": "19999"
                },
                {
                    "id": "15101",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d63d70854f76aeb62d11a71d5450d3ca.png",
                    "name": "Redmi X65 2022款 65英寸",
                    "price": "2799",
                    "priceOld": "3999"
                },
                {
                    "id": "15995",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205172334_e09d01f2ac097815342c53456c154c94.png",
                    "name": "小米电视EA58 2022款58英寸",
                    "price": "1499",
                    "priceOld": "1899"
                },
                {
                    "id": "15996",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205172345_362b9177a149ecce6af907565a7f7f52.png",
                    "name": "小米电视EA60 2022款60英寸",
                    "price": "1799",
                    "priceOld": "2099"
                },
                {
                    "id": "15900",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ff8e314fd89f8635cbd8e4b9bcb39571.png",
                    "name": "Redmi智能电视A58 2022款58英寸",
                    "price": "1299",
                    "priceOld": "1999"
                },
                {
                    "id": "16547",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205191526_4ecc555c5227bb335750dc3897f83491.png",
                    "name": "Redmi AI智能电视 X55 55英寸",
                    "price": "1499",
                    "priceOld": "1999"
                },
                {
                    "id": "16009",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205191526_4ecc555c5227bb335750dc3897f83491.png",
                    "name": "Redmi AI智能电视 X65 65英寸",
                    "price": "1999",
                    "priceOld": "2999"
                },
                {
                    "id": "15421",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d04ebee3b15421ad0f446b63f521d88c.png",
                    "name": "小米电视ES50 2022款 50英寸",
                    "price": "2199",
                    "priceOld": "2399"
                },
                {
                    "id": "14418",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a2cfbf298280d05b2fb3a482ac5d8009.png",
                    "name": "小米电视ES65 2022款65英寸 ",
                    "price": "3399",
                    "priceOld": "3999"
                },
                {
                    "id": "15501",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202211112258_4a860acbb234df9db2787518d7bb66fd.jpg",
                    "name": "小米电视EA70 2022款70英寸",
                    "price": "2299",
                    "priceOld": "3299"
                },
                {
                    "id": "13935",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/c6798961ac1540a07277a12183af727e.png",
                    "name": "小米电视EA50 2022款50英寸 ",
                    "price": "1299",
                    "priceOld": "1899"
                },
                {
                    "id": "16130",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202206071132_e19d427645bda2aeed5af44d2d1db330.png",
                    "name": "小米电视 EA Pro 65英寸 ",
                    "price": "2299",
                    "priceOld": "2999"
                },
                {
                    "id": "11855",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d344287147f188cbee0a94a81cf6d40a.png",
                    "name": "小米全面屏电视Pro 75英寸 E75S",
                    "price": "4499",
                    "priceOld": "4999"
                },
                {
                    "id": "10767",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/09fb0c54e6fff3fd9597f1dae663131c.png",
                    "name": "小米全面屏电视Pro 55英寸 E55S",
                    "price": "2399",
                    "priceOld": "2599"
                },
                {
                    "id": "16672",
                    "img": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202210192131_ac9020a4a9f1e08cd36558241925fc42.jpg",
                    "name": "小米电视 ES70 70英寸",
                    "price": "3999",
                    "priceOld": "4499"
                }
            ]
        }
    ],
    keywordList: ["灯具", "旅行箱", "手机贴膜"],
}