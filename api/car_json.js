module.exports = {
    "recommendList": [
        {
            "id": "5006",
            "img": "//cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/7c0adc9810058a7719a48394cc7d252d.jpg",
            "name": "小米无线开关",
            "price": "39",
            "priceOld": "39"
        },
        {
            "id": "9836",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a6a8abf50e2134139e24c30c9293871c.jpg",
            "name": "小米巨能写中性笔10支装",
            "price": "9.99",
            "priceOld": "9.99"
        },
        {
            "id": "15965",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205240036_9a8fbf274c0d419c5a0a49ef67cdd7c8.png",
            "name": "小米手环7",
            "price": "229",
            "priceOld": "249"
        },
        {
            "id": "15974",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205202109_e8c74026176502da03d220b2656896f9.png",
            "name": "Xiaomi激光打印一体机K200",
            "price": "1499",
            "priceOld": "1499"
        },
        {
            "id": "11190",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/d002490b5056c5eeef97bb7b96603133.jpg",
            "name": "小米米家蓝牙温湿度计 2",
            "price": "29",
            "priceOld": "29"
        },
        {
            "id": "13211",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eeb0b75372a5bdb433933c624b95a182.jpg",
            "name": "小米巨能写多彩中性笔",
            "price": "9.99",
            "priceOld": "9.99"
        },
        {
            "id": "15966",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205240036_9a8fbf274c0d419c5a0a49ef67cdd7c8.png",
            "name": "小米手环7 NFC版",
            "price": "269",
            "priceOld": "299"
        },
        {
            "id": "12195",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/29e845ec1ba0c3fd430c4fda8982223e.jpg",
            "name": "小米米家喷墨打印一体机",
            "price": "599",
            "priceOld": "599"
        },
        {
            "id": "9848",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c70d927ca6b19473bf0b5b74d68da7e2.jpg",
            "name": "小米中性笔",
            "price": "24.9",
            "priceOld": "24.9"
        },
        {
            "id": "11174",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3eead59acc600bf06eb38899a08bfa09.jpg",
            "name": "小米移动电源3 20000mAh USB-C双向快充版",
            "price": "129",
            "priceOld": "129"
        },
        {
            "id": "13128",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/9212a657c8a0610bf525aea4388d6b8e.jpg",
            "name": "小米米家照片打印机1S",
            "price": "599",
            "priceOld": "599"
        },
        {
            "id": "15964",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/nr-pub/202205211440_011c1ed4a7a41dab50eee1925c969d05.png",
            "name": "Redmi Note 11SE",
            "price": "1099",
            "priceOld": "1099"
        },
        {
            "id": "15099",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8282b858f91fac92aa30be6999335e9f.jpg",
            "name": "Redmi 手表 2",
            "price": "379",
            "priceOld": "399"
        },
        {
            "id": "15654",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e61c33af7a3fca13b0d58b80be100ec5.jpg",
            "name": "米家追光氛围灯带",
            "price": "299",
            "priceOld": "299"
        },
        {
            "id": "7530",
            "img": "//i8.mifile.cn/b2c-mimall-media/72f7329a96a5c89bf160fba8cd2745be.jpg",
            "name": "知吾煮汤锅 米家定制",
            "price": "99",
            "priceOld": "99"
        },
        {
            "id": "9542",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/4fcc8cd115c4d1417d8cb7860fe66049.jpg",
            "name": "小米米家电子温湿度计Pro",
            "price": "99",
            "priceOld": "99"
        },
        {
            "id": "10722",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/15ffaa4980abd25869035916281880ce.jpg",
            "name": "小米口袋照片打印机",
            "price": "349",
            "priceOld": "349"
        },
        {
            "id": "11041",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/110329c5dbee34348915d3b74fc6d1c4.jpg",
            "name": "米家吸顶灯",
            "price": "249",
            "priceOld": "249"
        },
        {
            "id": "11448",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3f565b0aaedeb7154d6443c628043cd9.jpg",
            "name": "米家即热饮水机C1",
            "price": "249",
            "priceOld": "249"
        },
        {
            "id": "12256",
            "img": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a6ed533c0a74e3d2889804c1290df557.jpg",
            "name": "小米移动电源3 30000mAh 快充版",
            "price": "199",
            "priceOld": "199"
        }
    ]
}