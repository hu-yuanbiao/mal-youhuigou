const HTTP = require('./request')

export function getSearchDiscover() {
    return HTTP({
        url: '/hisearch/se_default',
        method: 'get',
    })
}

// query=投影
export function getSearchSuggest(data) {
    return HTTP({
        url: '/hisearch/suggestion_v3',
        method: 'get',
        data
    })
}