const HTTP = require("./request");


module.exports = {
  "getCatagoryData": function (id) {
    return HTTP({
      url: "/home/category_v2",
      method: "get",
      data: {
        "cat_id": id,
      }
    })
  },
}