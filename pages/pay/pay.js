// pages/pay/pay.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        total: 0,
        oldTotal: 0,
        goodsList: [
            // {
            //   id:1,
            //   name:"小米手机",
            //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
            //   price:"2999",
            //   priceOld:"3999",
            //   num:1,
            //   checked:false
            // },
            // {
            //   id:2,
            //   name:"小米手机2",
            //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
            //   price:"2999",
            //   priceOld:"3999",
            //   num:1,
            //   checked:false
            // },
        ],
        addr: null,
        userAddr: [],
        selectedId: 0,
        popFlag: false,
    },

    // 计算总价格
    totalData() {
        let num = 0;
        let data = 0;
        let _data = 0;
        this.data.goodsList.forEach((item) => {
            if (item.checked) {
                num += 1;
                data += item.price * item.num;
                _data += item.priceOld * item.num;
            }
        })
        this.setData({
            total: data,
            oldTotal: _data,
            goodsNum: num
        })
    },

    // 跳转订单页
    goOrder() {
        if (!this.data.addr) return wx.showToast({ title: '先填写地址', icon: 'error' });
        wx.showModal({
            title: '支付商品',
            success: (res) => {
                if (res.confirm) {
                    console.log('用户点击确定')
                    wx.showLoading({ title: '加载中' })
                    this.setOrderList(2);//生成订单--待发货
                    setTimeout(() => {
                        wx.hideLoading()
                        wx.redirectTo({ url: '/pages/order/order' })
                    }, 1000)
                } else if (res.cancel) {
                    // console.log('用户点击取消')
                    wx.redirectTo({ url: '/pages/order/order' })
                    this.setOrderList(1);//生成订单--待支付
                }

            }
        })
    },

    // 生成订单
    // 1.待支付 2.待发货 3.待收货 4.待评价
    setOrderList(type) {

        let { addr: address } = this.data;

        let date = new Date();
        let time = date.getTime();
        let id = parseInt(time / date.getFullYear() / date.getMilliseconds());

        // 构造订单数据
        let orderData = {
            type,
            address,
            orderId: id,
            orderLists: this.data.goodsList,//商品列表
            total: this.data.total,//总价格
            createTime: time,// 订单创建时间
            expireTime: time + 1000 * 60 * 5,//过期时间
            newTime: time,// 当前时间
            payTIme: type > 1 ? new Date().getTime() : null, // 支付时间
            isShow: true,// 删除时使用
        };

        // 1.判断缓存有没有数据
        let goodsOrderList = wx.getStorageSync('goodsOrderList');
        if (goodsOrderList) {
            // 2.有数据 添加数据
            goodsOrderList.unshift(orderData);
            wx.setStorageSync('goodsOrderList', goodsOrderList)

        } else {
            // 3.没有数据 添加订单
            wx.setStorageSync('goodsOrderList', [orderData])
        }


        // 删除订单数据
        let goodsCarList = wx.getStorageSync('goodsCarList');
        let newlists = goodsCarList.filter(item => !item.checked); //过滤加入订单数据
        wx.setStorageSync('goodsCarList', newlists)

    },

    // 默认地址初始化
    addrInit() {
        let userAddr = wx.getStorageSync("userAddr");
        if (userAddr) {
            let addr = userAddr.find(item => item.isDefault);
            this.setData({ addr, userAddr, selectedId: addr.id })
        }
    },

    // 获取缓存数据
    reLoadGoodsList() {
        // 获取缓存数据
        let goodsCarList = wx.getStorageSync('goodsCarList');
        let dataLists = goodsCarList.filter(item => item.checked);
        this.setData({
            goodsList: dataLists
        })

        this.totalData();//计算总价格
    },

    // 获取默认地址
    reLoadAddr() {
        let userAddr = wx.getStorageSync("userAddr");
        if (userAddr) {
            let addr = userAddr.find(item => item.isDefault);
            this.setData({ addr, userAddr, selectedId: addr.id })
        }
    },

    // 切换地址
    toggleFun(e) {
        // console.log(e);
        let id = e.currentTarget.dataset.id;
        let { userAddr } = this.data;
        let addr = userAddr.find(item => item.id == id)
        this.setData({ selectedId: id, addr, popFlag: false });
    },

    // 显示弹窗
    showPop() {
        this.setData({ popFlag: true })
    },

    // 阻止冒泡
    stopProgetion() { },

    // 隐藏弹窗
    hidePop() {
        this.setData({ popFlag: false })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.addrInit();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

        // 获取缓存数据
        this.reLoadGoodsList();

        // 获取默认地址
        this.reLoadAddr();

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})