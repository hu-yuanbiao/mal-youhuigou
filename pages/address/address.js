// pages/address/address.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

        // 地址列表
        list: [
            // {
            //   "id": 1,
            //   "name": "11111111",
            //   "tel": "11111111111111",
            //   "addr": "1111111111111",
            //   "isDefault": true
            // },
            // {
            //   "id": 2,
            //   "name": "22222",
            //   "tel": "222222222222",
            //   "addr": "222222222222222",
            //   "isDefault": false
            // }
        ],

        // 弹窗状态
        popShow: false,

        // 功能 0是添加 1是修改
        popWay: 0,

        edit_id: 0,

        // 输入框内容
        inp_name: "",
        inp_tel: "",
        inp_addr: "",
    },

    // 添加地址第一步 显示弹窗
    add1() {
        this.setData({ popShow: true, inp_name: "", inp_tel: "", inp_addr: "", popWay: 0 });
    },

    // 取消第一步 隐藏弹窗
    cencalAdd1() {
        this.setData({ popShow: false });
    },

    // 阻止冒泡
    stopProgation() {
        // console.log('阻止冒泡');
    },

    // 输入框实时更新
    inpEve(e) {
        // console.log(e.currentTarget.dataset.key);
        // console.log(e.detail.value);
        this.setData({ [e.currentTarget.dataset.key]: e.detail.value })
    },

    // 添加地址第二步 push数据
    add2() {

        let { inp_name, inp_tel, inp_addr, list } = this.data
        // console.log(inp_name, inp_tel, inp_addr);

        if (
            /^.{3,20}$/.test(inp_name) &&
            /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/.test(inp_tel) &&
            /^.{3,20}$/.test(inp_addr)
        ) {

            list.push({
                id: list.length + 1,
                name: inp_name,
                tel: inp_tel,
                addr: inp_addr,
                isDefault: !list.length ? true : false,
            })

            // 更新数据并清空输入框
            this.setData({ list, inp_name: "", inp_tel: "", inp_addr: "", popShow: false });
        } else {
            wx.showToast({ icon: "error", title: '请检查' })
        }
    },

    // 地址列表初始化
    listInit() {
        let list = wx.getStorageSync('userAddr')
        if (!list) list = [];
        this.setData({ list })
    },

    // 设为默认地址
    setDufault(e) {
        // console.log(e.currentTarget.dataset.flag);
        let flag = e.currentTarget.dataset.flag;
        let id = e.currentTarget.dataset.id;
        let { list } = this.data
        if (!flag) {
            let _index = list.findIndex(item => item.isDefault);
            list[_index].isDefault = false;
            let index = list.findIndex(item => item.id == id);
            list[index].isDefault = true;
            this.setData({ list });
        }
    },

    // 删除功能
    delFun(e) {
        let id = e.currentTarget.dataset.id;
        let { list } = this.data
        let index = list.findIndex(item => item.id == id);
        // 如果删除的是默认
        if (list[index].isDefault) {
            list.splice(index, 1);
            if (list.length) list[0].isDefault = true;
        } else list.splice(index, 1);
        this.setData({ list })
        wx.showToast({ title: '删除成功' })
    },

    // 编辑功能第一步 显示弹窗 输入框自动有文字
    edit1(e) {
        let id = e.currentTarget.dataset.id;
        let { list } = this.data
        let index = list.findIndex(item => item.id == id);
        let { name, tel, addr } = list[index];
        this.setData({
            inp_name: name,
            inp_tel: tel,
            inp_addr: addr,
            popShow: true,
            popWay: 1,
            edit_id: id,
        })
    },

    // 编辑功能第二部 更新数据
    edit2() {
        let { inp_name, inp_tel, inp_addr, list, edit_id } = this.data
        // console.log(inp_name, inp_tel, inp_addr);
        let index = list.findIndex(item => item.id == edit_id);
        list[index].name = inp_name;
        list[index].tel = inp_tel;
        list[index].addr = inp_addr;

        if (
            /^.{3,20}$/.test(inp_name) &&
            /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/.test(inp_tel) &&
            /^.{3,20}$/.test(inp_addr)
        ) {
            // 更新数据并清空输入框
            this.setData({ list, inp_name: "", inp_tel: "", inp_addr: "", popShow: false });
        } else {
            wx.showToast({ icon: "error", title: '请检查' })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        // 地址列表初始化
        this.listInit();

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
        wx.setStorageSync('userAddr', this.data.list);
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})