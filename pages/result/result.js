// pages/result/result.js
let { getResultData } = require('../../api/result.js')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        text: "手机",//关键字
        is: 1,
        flag: false,
        screenShow: false,
        topShow: false,
        topShows: false,
        actionItem: "",
        allItem: '',
        tabItem: "",
        iconNum: 1,
        catoryList: ['小米电脑', '笔记本', '小米手机', '红米笔记本', '电视', '小米电视'],
        catoryList2: ['显示器', '主板维修', '电池更换服务', '至尊版', '5G', '免息'],
        con_i: null,
        textArr: '',
        cons_i: null,
        textsArr: "",
    },

    // 获取数据
    getResultDataFun() {
        getResultData({ query: this.data.text }).then(data => {
            let arr = []
            data.data.list_v2.forEach(item => {
                arr.push(item)
            })
            arr.sort((a, b) => {
                return b.body.comments_total - a.body.comments_total;
            });
            this.setData({
                actionItem: data.data.list_v2,
                allItem: data.data.list_v2,
                tabItem: data.data.classes
            })
            console.log(data);


        })
    },
    conIndex(e) {
        console.log(e.currentTarget.dataset.con);
        console.log(this.data.catoryList[e.currentTarget.dataset.con]);
        this.setData({
            con_i: e.currentTarget.dataset.con,
            textArr: this.data.catoryList[e.currentTarget.dataset.con]
        })

    },

    // 点击确定
    enter() {
        console.log(this.data.textArr, this.data.textsArr);
        this.setData({
            text: this.data.textsArr + this.data.textArr,
            screenShow: false
        })
    },

    consIndex(e) {
        console.log(e.currentTarget.dataset.con);
        console.log(this.data.catoryList2[e.currentTarget.dataset.con]);
        this.setData({
            cons_i: e.currentTarget.dataset.con,
            textsArr: this.data.catoryList2[e.currentTarget.dataset.con]
        })
    },
    commClick(e) {
        this.setData({
            text: this.data.tabItem[e.currentTarget.dataset.comm].class_name
        })
        this.getResultDataFun()
    },
    icon(e) {
        // console.log(e.target.dataset.icon);
        this.setData({
            iconNum: e.target.dataset.icon
        })
    },
    // 去详情页
    goto(e) {
        console.log(e);
    },

    tabFun(e) {

        this.setData({
            is: e.target.dataset.i
        })

        if (this.data.is == 2) {
            let list = []
            this.data.allItem.forEach(item => {
                list.push(item)
            })
            list.sort((a, b) => {
                return b.body.comments_total - a.body.comments_total;
            });
            this.setData({
                actionItem: list
            })
        } else if (this.data.is == 1) {
            this.setData({
                actionItem: this.data.allItem
            })
        }
    },
    tabsFun(e) {
        // console.log(e.target.dataset.i);
        let show = !this.data.flag
        this.setData({
            is: e.target.dataset.i,
            flag: show
        })
        if (this.data.flag == false) {
            let list = []
            this.data.allItem.forEach(item => {
                list.push(item)
            })
            list.sort((a, b) => {
                return b.body.price - a.body.price;
            });
            this.setData({
                actionItem: list
            })
        } else {
            let list = []
            this.data.allItem.forEach(item => {
                list.push(item)
            })
            list.sort((a, b) => {
                return a.body.price - b.body.price;
            });
            this.setData({
                actionItem: list
            })
        }


    },

    // 点击筛选
    screen() {
        this.setData({
            screenShow: true
        })
    },
    screenOut() {
        this.setData({
            screenShow: false
        })

    },

    // 收起多余数据
    upTop() {
        this.setData({
            topShow: true
        })
    },
    lower() {
        this.setData({
            topShow: false
        })
    },
    // 收起多余数据
    upTops() {
        this.setData({
            topShows: true
        })
    },
    lowers() {
        this.setData({
            topShows: false
        })
    },

    screenItem() {
        console.log(2);
    },

    // 清空输入框。
    // delFun() {
    //   this.setData({ inp_value: '', recommend_list: [] })
    // },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        // 拿到路由参数
        let { text } = options;
        // console.log(text);

        // 防止为空 更新搜索关键字
        if (text) this.setData({ text });

        // 初始化搜索结果
        this.getResultDataFun();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})