// pages/order/order.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        orderLists: [], // 订单数据
        index: 0, // 下标
        entpyFlag: false, // 空状态
        interval: null, // 定时器

        btnLeftList: [],//筋斗云使用
        bg_left: 0,
        bg_width: 0,
    },

    // 修改下标
    setIndex(event) {
        // console.log(event);
        let index = event.target.dataset.index;
        let { orderLists, btnLeftList } = this.data;
        // console.log(index, btnLeftList[index]);
        let entpyFlag = index != 0 && !orderLists.some(item => item.type == index);

        if (!entpyFlag && orderLists.every(item => !item.isShow)) entpyFlag = true;

        this.setData({
            index,
            entpyFlag,
            bg_left: btnLeftList[index].left,
            bg_width: btnLeftList[index].width,
        })
    },

    // 前往支付
    payFun(e) {
        wx.showModal({
            title: '支付商品',
            success: (res) => {
                if (res.confirm) {
                    wx.showLoading({ title: '加载中' })
                    setTimeout(() => {
                        wx.hideLoading()
                        let id = e.currentTarget.dataset.id;
                        this.setOrderIndex(id, 2)
                        wx.showToast({ title: '支付商品成功' });
                    }, 2000)
                } else if (res.cancel) {
                    wx.showLoading({ title: '加载中' })
                    wx.showToast({ icon: 'error', title: '支付商品失败' });
                    wx.hideLoading()
                }

            }
        })
    },

    // 催发货
    urgeFun(e) {
        // console.log(e.currentTarget.dataset.id);
        wx.showLoading({ title: '加载中' })
        setTimeout(() => {
            wx.hideLoading()
            let id = e.currentTarget.dataset.id;
            this.setOrderIndex(id, 3);
            wx.showToast({ title: '催发货成功' });
        }, 1000)
    },

    // 确认收货
    confirmFun(e) {
        // console.log(e.currentTarget.dataset.id);
        wx.showLoading({ title: '加载中' })
        setTimeout(() => {
            wx.hideLoading()
            let id = e.currentTarget.dataset.id;
            this.setOrderIndex(id, 4);
            wx.showToast({ title: '确认收货' });
        }, 1000)
    },

    // 待评价
    evaluateFun(e) {
        wx.showLoading({ title: '加载中' })
        setTimeout(() => {
            wx.hideLoading()
            let id = e.currentTarget.dataset.id;
            this.setOrderIndex(id, 0);
            wx.showToast({ title: '评价成功' });
        }, 1000)
    },

    // 修改状态
    setOrderIndex(id, index) {
        let { orderLists } = this.data;
        let i = orderLists.findIndex(item => item.orderId == id);
        orderLists[i].type = index;
        if (index == 2) orderLists[i].payTime = new Date().getTime();
        this.setData({ orderLists });
    },

    // 开启定时器
    openInterval() {
        let intervalFun = () => {
            // 需要定时的数据
            let { orderLists } = this.data;
            // console.log(orderLists);
            orderLists.forEach(item => {
                if (!item.payTime && item.type == 1) {
                    let index = orderLists.findIndex(obj => item.orderId == obj.orderId)
                    orderLists[index].newTime = new Date().getTime();
                    if (new Date().getTime() > orderLists[index].expireTime)
                        orderLists[index].type = -1;
                }
            });
            this.setData({ orderLists });
            // console.log(this.data);
        }
        this.setData({ interval: setInterval(intervalFun, 1000) });
    },

    // 关闭定时器
    closeInterval() {
        this.setData({ interval: null })
    },

    // 删除功能
    delFun(e) {
        wx.showLoading({ title: '加载中' })
        setTimeout(() => {
            wx.hideLoading()
            let id = e.currentTarget.dataset.id;
            let { orderLists } = this.data;
            let index = orderLists.findIndex(item => item.orderId == id);
            orderLists[index].isShow = false;
            this.setData({ orderLists })
            wx.showToast({ title: '删除成功' });
        }, 1000)
    },

    // 筋斗云使用
    btnLeftListInit(index) {
        // navOrder-box
        wx.createSelectorQuery()
            .selectAll('.navOrder-box .item').boundingClientRect().exec(rect => {
                // console.log(rect);
                let btnLeftList = rect[0].map(item => ({ left: item.left, width: item.width }));
                console.log(btnLeftList);
                this.setData({
                    btnLeftList,
                    bg_left: btnLeftList[index].left,
                    bg_width: btnLeftList[index].width,
                })
            })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // 结构options
        let { index } = options;

        // 默认是0
        if (!index) index = 0;

        // 修改下标
        this.setData({ index })

        // 筋斗云使用
        this.btnLeftListInit(index);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        let orderLists = wx.getStorageSync('goodsOrderList');
        if (!orderLists) orderLists = [];
        let { index } = this.data;
        let entpyFlag = index != 0 && !orderLists.some(item => item.type == index);

        if (!entpyFlag && orderLists.every(item => !item.isShow)) entpyFlag = true;
        // console.log(entpyFlag);
        this.setData({ orderLists, entpyFlag })

        // 开启定时器
        this.openInterval();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
        // 关闭定时器
        this.closeInterval();
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
        // 关闭所有页面，打开到应用内的某个页面
        wx.setStorageSync('goodsOrderList', this.data.orderLists)
        // wx.reLaunch({
        //     url: '/pages/my/my',
        // })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})