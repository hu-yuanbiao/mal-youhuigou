// pages/details/details.js

// 静态数据
const { data: DATA } = require("../../api/details.js");

// 请求api
const { getMiproduct } = require("../../api/details.js");

Page({

    /**
     * 页面的初始数据
     */
    data: {
        swiperHeight: 200,
        tabs: 1,//切换功能
        num: 0,//购物车数量
        goodsDetails: null,
        imgs: [],
        current: 1,//轮播图位置
        goods: {
            id: 5,
            goodsImage: "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/abdb63b033c128fb5b8b624865048a9b.jpg",
            goodsName: "小米手机",
            goodsPrice: "2999",
            goodsPriceOld: "3999",
        }
    },


    // 加入购物车
    setCarNum() {

        let { id, goodsImage, goodsName, goodsPrice, goodsPriceOld } = this.data.goods;
        // 购物车数据结构
        let carLists = {
            id: id,
            name: goodsName,
            img: goodsImage,
            price: goodsPrice,
            priceOld: goodsPriceOld,
            num: 1,
            checked: true
        };

        // 1.判断数据缓存种是否存在
        let goodsCarList = wx.getStorageSync('goodsCarList');
        if (goodsCarList) {
            // 1.有数据判断数据存在
            let index = goodsCarList.findIndex(item => item.id == this.data.goods.id);
            // 没有相同商品，添加数据
            if (index == -1) {
                goodsCarList.push(carLists);
            } else {
                // 有相同商品，数量加一
                goodsCarList[index].num += 1;
            }
            wx.setStorageSync('goodsCarList', goodsCarList)

        } else {
            // 2.没有数据，添加数据
            wx.setStorageSync('goodsCarList', [carLists])

        }

        this.setData({
            num: wx.getStorageSync('goodsCarList').length
        })

    },

    // 切换功能
    isTabShow(e) {
        let index = e.target.dataset.index;
        this.setData({
            tabs: index
        })
    },

    // 轮播图下标 修改
    bindchangeFun(e) {
        this.setData({
            current: e.detail.current + 1
        })
    },

    // 获取图片高度
    bindloadImg(e) {
        let { height, width } = e.detail;
        // 获取硬件系统信息
        wx.getSystemInfo({
            success: (res) => {
                this.setData({
                    swiperHeight: res.windowWidth / width * height
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // console.log(options.id);
        wx.showLoading({ title: '加载中' });
        let param_id = options.id;
        if (!param_id) param_id = 9836;
        getMiproduct({ "commodity_id": param_id }).then(data => {
            // console.log(data);
            // let data = getMiproduct;
            if (!data) { data = DATA; wx.hideLoading({ noConflict: true }); return; };

            let goods_info = data.data.goods_info[0];
            let goods_tpl_datas = data.data.goods_tpl_datas[goods_info.page_id].sections;
            let product_info_tab_index = goods_tpl_datas.findIndex(item => item.view_type == "product_info_tab");
            // console.log(product_info_tab_index);
            console.log("=======>", goods_tpl_datas[product_info_tab_index].body.items);
            this.setData({
                imgs: data.data.goods_share_datas.gallery_view.slice(0, 4),
                "product_info": data.data.product_info,
                "goodsDetails": goods_tpl_datas[product_info_tab_index].body.items,
                "goods.id": goods_info.product_id,
                "goods.goodsImage": goods_info.img_url,
                "goods.goodsName": goods_info.name,
                "goods.goodsPrice": goods_info.price,
                "goods.goodsPriceOld": goods_info.market_price,
            })
            wx.hideLoading({ noConflict: true });
        }).catch(() => { wx.hideLoading({ noConflict: true }); wx.showToast({ icon: "error", title: '请重试' }) })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

        console.log("onReady");
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        console.log("onShow");
        this.setData({
            num: wx.getStorageSync('goodsCarList').length
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
        console.log("onHide");

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})

