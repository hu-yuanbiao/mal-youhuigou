// pages/search/search.js
let { getSearchDiscover, getSearchSuggest } = require('../../api/search.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    discover_list: [],//搜索发现列表
    history_list: [],//搜索历史列表
    inp_value: '',//搜索框内容
    request_flag: true,//请求标识
    recommend_list: [],//搜索建议列表
  },

  async getSearchDiscoverInit() {
    let data = await getSearchDiscover().catch(() => false);
    console.log(data);

    if (data) this.setData({ discover_list: data.data.discover_list })
    else wx.showToast({ title: '失败', icon: 'error', duration: 1000 })
  },

  getSearchHistoryInit() {
    let searchHistory = wx.getStorageSync('searchHistory');
    if (!searchHistory) searchHistory = [];
    this.setData({ history_list: searchHistory })
  },

  // 清空输入框
  delFun() {
    this.setData({ inp_value: '', recommend_list: [] })
  },

  // 输入框输入
  inpEve(e) {
    // console.log("inpEve",e.detail.value);

    let inp_value = e.detail.value

    // 输入框实时更新数据
    this.setData({ inp_value: inp_value });

  },

  // 搜索提交 
  subFun() {
    // console.log("subFun", this.data.inp_value);
    let val = this.data.inp_value;
    this.requireFun(val)
  },

  // 请求搜索建议
  requireFun(inp_value) {
    // 判断请求标识
    if (this.data.request_flag) {

      // 反转请求标识
      this.setData({ request_flag: false });

      getSearchSuggest({ query: inp_value }).then(data => {

        let recommend_list = data.data.list.map(item => {
          item.title = item.title.replace(inp_value, "");
          return item
        })
        // console.log(recommend_list);

        // 反转请求标识 更新数据
        this.setData({
          request_flag: true,
          recommend_list,
        });

        if(!recommend_list.length) wx.showToast({ title: '没有数据', icon: 'error', duration: 1000 })
        let searchHistory = this.data.history_list;
        if (searchHistory.some(_inp_value => inp_value == _inp_value)) return;
        searchHistory.push(inp_value);
        this.setData({ history_list: searchHistory });

      }).catch(() => { wx.showToast({ title: '失败', icon: 'error', duration: 1000 }) })

    }
  },

  // 替换输入框内容
  setInpVal(e) {
    // console.log(e.currentTarget.dataset.val);
    let val = e.currentTarget.dataset.val;
    this.setData({ inp_value: val })
  },

  // 清空历史搜索
  clearFun() {
    this.setData({ history_list: [] })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getSearchDiscoverInit()
    this.getSearchHistoryInit()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    wx.setStorageSync('searchHistory', this.data.history_list)
  },

  /** 
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    wx.setStorageSync('searchHistory', this.data.history_list)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})