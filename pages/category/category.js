// pages/category/category.js

// 备用本地数据
let {
    catagoryList: CL,
    category_name: CN
} = require("../../api/category_json");

let {
    getCatagoryData
} = require("../../api/category");


Page({

    /* 页面的初始数据 */
    data: {
        itemIndex: 0,
        catagoryList: null, // 所有列表
        category_name: null, // 左侧分类
        tabsIndex: 0, // 右侧tabs
        offsetHeightList: [], // 获取所有dom
        s_top: 0, // 控制右侧滚动条位置
        isComputed: true, // 重新计算top
        scrollFlag: true, // 防抖
    },

    // 左侧切换效果
    setItem(event) {
        wx.pageScrollTo({
            scrollTop: 0,
            duration: 100
        })
        let index = event.currentTarget.dataset.index;
        this.setData({
            itemIndex: index,
            tabsIndex: 0,
            s_top: 0,
        })

    },

    // 右侧tabs切换
    tabs_item(e) {
        let tabsIndex = e.currentTarget.dataset.index;
        this.setData({
            tabsIndex,
            s_top: this.data.offsetHeightList[tabsIndex]
        })
        console.log(this.data.tabsIndex);

    },

    // 获取数据
    getCatagoryDataFun() {
        getCatagoryData().then(data => {

            // 过渡动画
            wx.showLoading({ title: '加载中' })

            // 保守法静态数据
            if (!data) {
                this.setData({
                    catagoryList: CL,
                    category_name: CN,
                })
                wx.hideLoading({ noConflict: true })
                return;
            }

            let catagoryList = data.data;
            catagoryList.shift(); //删除首个

            let catagory1;
            let catagory2;
            let catagory3;

            // 请求最大数据中缺失的三个数据
            getCatagoryData(1243).then(data => {
                catagory1 = data.data[0];
            })
            getCatagoryData(1251).then(data => {
                catagory2 = data.data[0];
            })
            getCatagoryData(459).then(data => {
                catagory3 = data.data[0];

                // 
                let category_name = catagoryList.map((item) => {
                    return {
                        title: item.category_name
                    }
                })

                catagoryList = catagoryList.map((item, index) => {
                    // 修改对应三个缺失数据的数据
                    if (index == 1) {
                        item = catagory1
                    } else if (index == 2) {
                        item = catagory2
                    } else if (index == 3) {
                        item = catagory3
                    }
                    return item
                })


                this.setData({
                    catagoryList,
                    category_name,
                })

                wx.hideLoading({ noConflict: true })
                console.log(catagoryList);

                // 获取dom
                this.getOffsetHeight();
            })

        })
    },

    // 获取每个dom的offsetHeight
    getOffsetHeight() {
        var query = wx.createSelectorQuery()
        query.selectAll(".ul.type1").boundingClientRect().exec()
        query.selectAll(".ul.type2").boundingClientRect().exec(rect => {
            let offsetHeightList = rect[0].concat(rect[1]).map(item => {
                return item.top - 100
            })
            this.setData({
                offsetHeightList
            })
            // console.log(this.data.offsetHeightList);

        })
    },

    // 监听滚动条
    binddraggingFun(e) {

        // 防抖标志
        if (!this.data.scrollFlag) return;

        // 立即翻转防抖标志
        this.setData({ scrollFlag: false })

        // console.log(e.detail.scrollTop);

        // 拿到下标
        let tabsIndex = this.data.offsetHeightList.findIndex((item, i) => {
            if (item >= e.detail.scrollTop) {
                // console.log(i);
                return i + 1;
            }
        })

        // 立即修改
        this.setData({ tabsIndex });

        // 防抖延迟修改
        setTimeout(() => { this.setData({ scrollFlag: true }) }, 300)

    },

    // 左侧下标初始化
    indexInit() {
        let itemIndex = wx.getStorageSync('categoryIndex')
        if (!itemIndex) itemIndex = 0;
        this.setData({ itemIndex });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

        // 获取分类数据
        this.getCatagoryDataFun();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        // 左侧下标初始化
        this.indexInit();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})